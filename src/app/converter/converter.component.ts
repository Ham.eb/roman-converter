import { Component, OnInit } from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';

@Component({
  selector: 'app-converter',
  templateUrl: './converter.component.html',
  styleUrls: ['./converter.component.css'],
})
export class ConverterComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
  arabicArray = [
    10000,
    5000,
    4000,
    1000,
    900,
    500,
    400,
    100,
    90,
    50,
    40,
    10,
    9,
    5,
    4,
    1,
  ];
  romanArray = [
    'X-',
    'V-',
    'MV-',
    'M',
    'CM',
    'D',
    'CD',
    'C',
    'XC',
    'L',
    'XL',
    'X',
    'IX',
    'V',
    'IV',
    'I',
  ];

  // used for showing the result
  romanOut: string = 'Result will be here';

  onConvert(form: NgForm) {
    let arabic = form.value.number; // input value
    let roman = '';

    for (let index = 0; index < this.arabicArray.length; index++) {
      while (this.arabicArray[index] <= arabic) {
        roman += this.romanArray[index];
        arabic -= this.arabicArray[index];
        this.romanOut = roman;
      }
    }
  }
}
